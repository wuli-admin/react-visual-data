# data-visual-platform

#### 介绍

🎉 基于 reactjs 开发的可视化大屏设计器项目https://wuli-admin.gitee.io/react-visual-data/#/dashboard

#### 参与贡献

此项目为作者闲暇时间`学习计划`的一部分，项目中可能会出现实验性的功能，所以在完成第一个可用版本之前只接受 `fixbug PR`，不接受 `feature PR`。还望大家理解。

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

**目录结构：**

```
├── config-overrides.js
├── package-lock.json
├── package.json
├── public
│   ├── config.js
│   ├── favicon.ico
│   ├── index.html
│   ├── robots.txt
│   └── static
├── README.md
├── src
│   ├── api
│   ├── App.js
│   ├── components
│   ├── index.js
│   ├── layouts
│   ├── library
│   ├── materials
│   ├── pages
│   ├── polyfills.js
│   ├── styles
│   └── __mocks__
```
